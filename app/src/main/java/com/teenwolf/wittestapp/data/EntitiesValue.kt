package com.teenwolf.wittestapp.data

data class EntitiesValue(
    val confidence: Float?,
    val value: String?
)