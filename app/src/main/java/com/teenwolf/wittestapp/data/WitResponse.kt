package com.teenwolf.wittestapp.data

data class WitResponse(
    val _text: String?,
    val entities: Map<String, Array<EntitiesValue>>?,
    val msg_id: String?

)