package com.teenwolf.wittestapp

import com.teenwolf.wittestapp.data.WitResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface IWitApi {

    @Headers("Authorization: Bearer D63CDJVQ2YPBDBWZW2MSKQAVZ2EDKPFO")
    @GET("/message")
    fun getWitResponse(@Query(value = "q", encoded = true) q: String): Observable<WitResponse>

    companion object Factory {
        fun create(): IWitApi {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.wit.ai")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return retrofit.create(IWitApi::class.java)
        }
    }
}