package com.teenwolf.wittestapp.repositories

import com.teenwolf.wittestapp.IWitApi
import com.teenwolf.wittestapp.data.WitResponse
import io.reactivex.Observable

class WitRepository(private val witApi: IWitApi) {
    fun getResponse(msg: String): Observable<WitResponse> = witApi.getWitResponse(msg)
}