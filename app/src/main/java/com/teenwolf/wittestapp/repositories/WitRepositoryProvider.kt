package com.teenwolf.wittestapp.repositories

import com.teenwolf.wittestapp.IWitApi

object WitRepositoryProvider {
    fun provideWitRepository(): WitRepository = WitRepository(IWitApi.create())
}