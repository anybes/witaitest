package com.teenwolf.wittestapp

import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.teenwolf.wittestapp.repositories.WitRepositoryProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        btn.setOnClickListener {

            WitRepositoryProvider.provideWitRepository().getResponse("Включи свет")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        Log.v("Response text ", it._text)
                        it.entities?.values?.forEach { value ->
                            Log.v(
                                "Response value ",
                                value.first().value
                            )
                        }

                        textView.text = it.entities?.keys?.first().toString()
                    },
                    { it.printStackTrace() })
        }

    }


}






